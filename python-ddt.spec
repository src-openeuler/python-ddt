%global _empty_manifest_terminate_build 0
Name:           python-ddt
Version:        1.7.2
Release:        1
Summary:        Data-Driven/Decorated Tests
License:        MIT
URL:            https://github.com/datadriventests/ddt
Source0:        %{pypi_source ddt}
BuildArch:      noarch

%description
A library to multiply test cases

%package -n python3-ddt
Summary:        Data-Driven/Decorated Tests
Provides:       python-ddt
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:  python3-pytest
BuildRequires:  python3-six
BuildRequires:  python3-PyYAML
# General requires
BuildRequires:  python3-enum34
# General requires
Requires:       python3-enum34
%description -n python3-ddt
A library to multiply test cases

%package help
Summary:        Data-Driven/Decorated Tests
Provides:       python3-ddt-doc
%description help
A library to multiply test cases

%prep
%autosetup -n ddt-%{version}

%build
%py3_build

%install
%py3_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%check
grep -rn 'import aiounittest' | awk -F: '{print $1}' | rm -rf `xargs`
%{__python3} setup.py test

%files -n python3-ddt -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Aug 28 2024 Ge Wang <wang__ge@126.com> - 1.7.2-1
- Upgrade to 1.7.2

* Thu Sep 29 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 1.6.0-1
- Upgrade to 1.6.0

* Thu May 19 2022 renliang16 <renliang@uniontech.com> - 1.4.4-1
- Upgrade package python3-ddt to version 1.4.4

* Thu Nov 19 2020 Python_Bot <Python_Bot@openeuler.org> - 1.4.1-1
- Package Spec generated
